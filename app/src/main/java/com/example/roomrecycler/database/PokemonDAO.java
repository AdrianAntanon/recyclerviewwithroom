package com.example.roomrecycler.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface PokemonDAO {

    @Query("SELECT * FROM Pokemon")
    public List<Pokemon>getAll();

    @Insert
    public void insert(Pokemon pokemon);

    @Update
    public void update(Pokemon pokemon);

    @Delete
    public void delete(Pokemon pokemon);

    @Query("SELECT * FROM Pokemon WHERE id_pokemon=:idPokemon")
    public Pokemon findById(int idPokemon);

    @Query("DELETE FROM Pokemon")
    public void deleteAll();
}
