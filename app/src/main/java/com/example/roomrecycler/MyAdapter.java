package com.example.roomrecycler;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.roomrecycler.database.Pokemon;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    private List<Pokemon> pokemons;
    private int layout;
    private OnItemClickListener itemClickListener;

    public MyAdapter(List<Pokemon> pokemons, int layout, OnItemClickListener itemClickListener) {
        this.pokemons = pokemons;
        this.layout = layout;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(this.layout, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(this.pokemons.get(position), this.itemClickListener);

    }

    @Override
    public int getItemCount() {
        return this.pokemons.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView textViewName;
        public TextView textViewType;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.textViewName = itemView.findViewById(R.id.textViewName);
            this.textViewType = itemView.findViewById(R.id.textViewType);
        }

        public void bind(final Pokemon pokemon, final OnItemClickListener itemClickListener){
            this.textViewName.setText(pokemon.getName());
            this.textViewType.setText(pokemon.getType());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClickListener.onItemClick(pokemon, getAdapterPosition());
                }
            });
        }
    }
    public void setPokemons(List<Pokemon> pokemons){
        this.pokemons = pokemons;
        notifyDataSetChanged();
    }

    public interface OnItemClickListener {
        void onItemClick(Pokemon pokemon, int position);
    }
}
