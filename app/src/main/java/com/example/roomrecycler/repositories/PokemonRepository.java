package com.example.roomrecycler.repositories;

import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.roomrecycler.database.Pokemon;
import com.example.roomrecycler.database.PokemonDAO;

import java.util.List;

public class PokemonRepository {
    PokemonDAO dao;

    public PokemonRepository(PokemonDAO dao) {
        this.dao = dao;
    }

    public List<Pokemon> getAll(){
        return dao.getAll();
    }

    public void insert(Pokemon pokemon){
        dao.insert(pokemon);
    }

    public void update(Pokemon pokemon){
        dao.update(pokemon);
    }

    public void delete(Pokemon pokemon){
        dao.delete(pokemon);
    }

    public Pokemon findById(int idPokemon){
        return dao.findById(idPokemon);
    }

    public void deleteAll(){
        dao.deleteAll();
    }
}
