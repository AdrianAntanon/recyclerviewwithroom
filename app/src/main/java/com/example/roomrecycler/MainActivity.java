package com.example.roomrecycler;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.roomrecycler.database.AppDatabase;
import com.example.roomrecycler.database.Pokemon;
import com.example.roomrecycler.database.PokemonDAO;
import com.example.roomrecycler.repositories.PokemonRepository;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    static AppDatabase db;
    static PokemonDAO dao;
    static PokemonRepository repository;

    private RecyclerView recyclerView;
    private FloatingActionButton addButton;
    private List<Pokemon> pokemons;
    private MyAdapter myAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = AppDatabase.getInstance(this);
        dao = db.pokemonDAO();
        repository = new PokemonRepository(dao);

        recyclerView = findViewById(R.id.recyclerView);
        addButton = findViewById(R.id.addButton);

        pokemons = repository.getAll();

        myAdapter = new MyAdapter(pokemons, R.layout.item_view, new MyAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Pokemon pokemon, int position) {
//                    UPDATE
                repository.deleteAll();
            }
        });

        recyclerView.setAdapter(myAdapter);
        RecyclerView.LayoutManager layout = new LinearLayoutManager(this);

        recyclerView.setLayoutManager(layout);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                repository.insert(new Pokemon("Bulbasaur", "Planta"));
                pokemons = repository.getAll();
                myAdapter.setPokemons(pokemons);
//                myAdapter.notifyDataSetChanged();
            }
        });

    }
}